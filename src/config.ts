const { SALT ='test', API_PORT, DB_HOST, DB_PORT, DB_NAME } = process.env;

export {
  SALT,
  API_PORT,
  DB_HOST,
  DB_PORT,
  DB_NAME

};
export const tokenTime = '300h';

