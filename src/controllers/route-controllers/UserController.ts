import { JsonController, Param, Body, HttpCode, Get, Post, Put, Delete, HttpError, QueryParam, QueryParams } from 'routing-controllers';
import { AuthService } from '../../sevices';
import { IUser, UserParams } from '../../interfaces';

import { User, Message, ChatMember } from '../../models';
import * as Boom from 'boom';

import { UserPost } from '../../validators';
import { Transform } from 'stream';
const authService = new AuthService();
@JsonController()
export class UserController {
  @Get('/')
  @HttpCode(200)
  public some() {
    return 'test response';
  }
  @Get('/user')
  public async getAll() {
    const users = (await User.find()).map(user => user.toJSON());
    return users;
  }
  @Get('/user/:id')
  public async getOne(@Param('id') id: string) {
    const result = await User.findById(id);
    const user = result && result.toJSON();
    if (!user) {
      throw new HttpError(404, `User was not found.`);
    }
    return user;
  }
  @Get('/self')
  public async getUser(@QueryParams() {token}) {
    try {
      const { userId } = await authService.tokenDecode(token);
      const result = await User.findById(userId).then(res => res && res.toJSON());
      if (!result) {
        throw Boom.badRequest('error');
      }
      const { roles, ...user } = result;
      return user;
    } catch (e) {
      console.error(e);
      throw Boom.unauthorized('token expired');
    }
  }
  @Post('/validateEmail')
  public async check(@Body() email) {
    console.log(email);
    
    if ((await User.find(email)).length) {
      console.log('exist');
      
      return { exist: 'such email already exist' };
    }
    return null;
  }
  @Post('/user')
  @HttpCode(201)
  public async post(@Body() { password, ...user }: UserPost) {
    // console.log(user);

    if ((await User.find({ email: user.email })).length) {
      throw Boom.badRequest('User already exist');
    }
    user.roles = new Array();
    if (user.firstName === 'shadow') {
      user.roles.push('admin');
    } else {
      user.roles.push('user');
    }

    await new User({
      hash: authService.getHash(password),
      ...user
    }).save();
    const result = await User.findOne({ email: user.email });
    const { roles, ...currentUser } = result && result.toJSON();
    const token = authService.tokenEncode(currentUser._id);
    const response = { user: currentUser, token };
    // await new ChatMember({
    //   userId: currentUser._id,
    //   chats: []
    // }).save();
    return response;
  }

  @Put('/user/:id')
  public async put(@Param('id') id: string, @Body() user: IUser) {
    await User.findByIdAndUpdate(id, user);
    return 'user edited';
  }

  @Delete('/user/:id')
  public async remove(@Param('id') id: string) {
    await User.findByIdAndRemove(id);
    return 'user deleted';
  }
  @Delete('/test/:id')
  public async delete(@Param('id') id: string) {
    await Message.findByIdAndRemove(id);
    return 'message deleted';
  }
}
