import { OnConnect, SocketController, ConnectedSocket, OnDisconnect, MessageBody, OnMessage } from 'socket-controllers';

// import {Message }from '../../models'
import { IMessage } from '../../interfaces';

import { Message, User, Chat, ChatMember } from '../../models';

@SocketController('')
export class RoomController {
  @OnMessage('create chat')
  public async createChat(@ConnectedSocket() socket) {
    const currentUserId = socket.handshake.user._id;
    await new Chat({}).save().then(res => {
      new ChatMember({
        userId: currentUserId,
        chatId: res._id
      }).save();
    });
  }
  @OnMessage('join chat')
  public async joinChat(@ConnectedSocket() socket, @MessageBody() { currentChatId }) {
    const currentUserId = socket.handshake.user._id;
    await new ChatMember({
      userId: currentUserId,
      chatId: currentChatId
    }).save();
  }
  @OnMessage('get chats')
  public async getChats(@ConnectedSocket() socket) {
    console.log('get chats');
    const id = socket.handshake.user._id;
    await ChatMember.find({ userId: id }).then(res => {
      if (res) {
        // console.log(res);
        console.log('called get chats');
        

        socket.emit('here your chats', res);
      }
    });
  }
}
