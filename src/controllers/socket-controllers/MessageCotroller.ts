import { OnConnect, SocketController, ConnectedSocket, OnDisconnect, MessageBody, OnMessage, SocketIO, SocketId } from 'socket-controllers';

// import { Message, User} from '../../models';
// import { AuthService } from '../../sevices';

// const authService = new AuthService();
// let user;
@SocketController('')
export class MessageController {
  @OnMessage('new message')
  public async saveMessage(@ConnectedSocket() socket, @MessageBody() { message }) {
    socket.emit('message saved')
    console.log(message);
  }
  // @OnConnect()
  // public async connection(@ConnectedSocket() socket) {
  //   const msgs = (await Message.find()).map(test => test.toJSON());
  //   socket.emit('recieve_all_messages', msgs);
  //   console.log('client connected ');
  // }

  // @OnDisconnect()
  // public disconnect(@ConnectedSocket() socket: any) {
  //   if (user) {
  //     const userName = user.firstName;
  //     console.log(`${userName} disconnected`);
  //   } else {
  //     console.log('someone disconnected');
  //   }
  // }

  // @OnMessage('authorization')
  // public async test(@ConnectedSocket() socket, @MessageBody() { token }) {
  //   const { userId } = authService.tokenDecode(token);
  //   const result = await User.findById(userId);
  //   user = result && result.toJSON();

  //   if (user) {
  //     socket.handshake.user = user;
  //     const id = socket.handshake.user._id;
  //     const name = socket.handshake.user.firstName
  //     socket.emit('authorized');
  //     console.log(`${name} logged`);
  //   } else {
  //     console.log('auth failed');
  //   }
  // }
}
