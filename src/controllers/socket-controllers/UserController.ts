import { OnConnect, SocketController, ConnectedSocket, OnDisconnect, MessageBody, OnMessage } from 'socket-controllers';
import { authEvent } from './socketEvents';

import { Message, User } from '../../models';
import { AuthService } from '../../sevices';

const authService = new AuthService();
let user;
@SocketController('')
export class MessageController {
  @OnMessage(authEvent.CHECK_TOKEN)
  public async checkToken(@MessageBody() { token }) {
    console.log('hi');
    
  }
  @OnConnect()
  public async connection(@ConnectedSocket() socket) {
    console.log('client connected ');
    // socket.emit('connect')
  }

  @OnDisconnect()
  public disconnect(@ConnectedSocket() socket: any) {
    if (user) {
      const userName = user.firstName;
      console.log(`${userName} disconnected`);
    } else {
      console.log('someone disconnected');
    }
  }
  @OnMessage('authorization')
  public async test(@ConnectedSocket() socket, @MessageBody() { token }) {
    const { userId } = authService.tokenDecode(token);
    const result = await User.findById(userId);
    user = result && result.toJSON();

    if (user) {
      socket.handshake.user = user;
      const name = socket.handshake.user.firstName;
      socket.emit('authorized');
      console.log(`${name} logged`);
    } else {
      console.log('auth failed');
    }
  }
}
