export interface IChatMember {
  userId: string;
  chats: [string];
}
