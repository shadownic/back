import { Document } from 'mongoose';
import { IChatMember } from './ChatMember';

export type ChatMemberMudule = Document & IChatMember;
