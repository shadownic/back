import { Document } from 'mongoose';
import {IChat} from './Chat'

export type ChatModel = Document & IChat