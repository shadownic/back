export interface IMessage{
    id?: string,
    data: string,
    auth?: string,
    authId: string,
    createdAt?: Date
}