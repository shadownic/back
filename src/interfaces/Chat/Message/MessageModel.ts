import { Document } from 'mongoose';
import { IMessage } from './Message';

export type MessageModel = Document & IMessage;
