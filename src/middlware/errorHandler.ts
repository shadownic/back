import { Middleware, KoaMiddlewareInterface } from 'routing-controllers';

@Middleware({ type: 'before' })
export class CustomErrorHandler implements KoaMiddlewareInterface {
  public async use(context: any, next: (err?: any) => Promise<any>) {
    try {
      await next();
    } catch (e) {
      // console.error(e);

      let payload = e;
      if (e.errors) {
        e.errors.forEach(item => {
          payload.message = Object.values(item.constraints);
          payload.status = 405;
        });
      }

      if (e.isBoom) {
        // console.log(e);

        payload = e.output.payload;
        payload.data = e.data;
      }
      context.status = payload.status || payload.statusCode || 501;
      context.body = payload;
    }
  }
}
