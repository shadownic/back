import { UserModel, ChatModel, MessageModel, ChatMemberMudule } from '../interfaces';
import { userSchema, chatSchema, messageSchema, chatMemberSchema } from '../schemas';

import * as mongoose from 'mongoose';

export const User = mongoose.model<UserModel>('User', userSchema);
export const Chat = mongoose.model<ChatModel>('Chat', chatSchema);
export const Message = mongoose.model<MessageModel>('Message', messageSchema);
export const ChatMember = mongoose.model<ChatMemberMudule>('ChatMember', chatMemberSchema)