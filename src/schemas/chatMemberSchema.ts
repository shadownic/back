import { Schema } from 'mongoose';

export const chatMemberSchema: Schema = new Schema ({
    // userId: {type: Schema.Types.ObjectId, ref: 'User', childPath: ''},
    // chats: Array
    userId: {type: [String], index: true},
    chatId: {type: [String], index: true}
})
