import { Schema } from 'mongoose';
import { ChatModel } from '../interfaces';
import { Message } from '../models';

export const chatSchema: Schema = new Schema(
  {
  
  },
  {
    toJSON: {
      transform: (doc, ret) => {
        ret._id = ret._id.toJSON();
        delete ret.__v;
      }
    }
  }
);
