import { Schema } from 'mongoose';
import { MessageModel } from '../interfaces';

export const messageSchema: Schema = new Schema(
  {
    // createdAt: Date,
    text: String,
    authId: String,
    chatId: { type: String, index: true }
  },
  {
    toJSON: {
      transform: (doc, ret) => {
        ret._id = ret._id.toJSON();
        delete ret.__v;
      }
    }
  }
);
