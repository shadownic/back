import 'reflect-metadata'; // this shim is required
import { createKoaServer } from 'routing-controllers';
import { createSocketServer, useSocketServer } from 'socket-controllers';
import * as IO from 'socket.io';

import * as mongoose from 'mongoose';
import { AuthorizationChecker } from './middlware/AuthorizationChecker';

export const startServer = async (port, dbUrl) => {
  await mongoose.connect(dbUrl);
  const server = await createKoaServer({
    cors: true,
    routePrefix: '',
    defaultErrorHandler: false,
    middlewares: [__dirname + '/middlware/**/*.ts'],
    controllers: [__dirname + '/controllers/route-controllers/*.ts'],
    authorizationChecker: AuthorizationChecker
  }).listen(port);
  const io = IO(server, {
    transports: ['polling, websocket']
  });
  await useSocketServer(io, {
    controllers: [__dirname + '/controllers/socket-controllers/*.ts']
  }).listen(server);
};
